import * as types from './actionTypes';

export function changeClientName(newName) {
    return({ type: types.CHANGE_NAME, name: newName });
}

export function changeBirthday(newBirthday) {
    return({ type: types.CHANGE_BIRTHDAY, birthday: newBirthday });
}

export function changeFruit(newFruit) {
    return({ type: types.CHANGE_FRUIT, fruit: newFruit });
}