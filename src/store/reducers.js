import Immutable from 'seamless-immutable';

import * as types from './actionTypes';

const initialState = Immutable({
    clientName: "",
    clientFruit : "",
    clientBirthdate : new Date(),
    onBirthdayChange :  {}
});


export default function reduce(state = initialState, action = {}) {
    switch (action.type) {
        case types.CHANGE_NAME:
            return state.merge({
                clientName: action.name
            });
        case types.CHANGE_BIRTHDAY:
            return state.merge({
                clientBirthdate: action.birthday
            });
        case types.CHANGE_FRUIT:
            return state.merge({
                clientFruit: action.fruit
            });
        default:
            return state;
    }
}

// selectors
export function getSelectedName(state) {
    return state.clientName;
}
export function getSelectedFruit(state) {
    return state.clientFruit;
}
export function getSelectedBirthday(state) {
    return state.clientBirthdate;
}