import React, {Component} from 'react';
import autoBind from "react-autobind";
import * as constants from './constants';
import { Text } from "react-native";

export default class FruitInput extends Component {
    constructor(props) {
        super(props);
        autoBind(this);
    }

    handleChange(e) {
        this.props.onFruitChange(e.target.value);
    }

    render() {
        const name = this.props.clientName;
        const selectedFruit = this.props.clientFruit;
        const nameInStyle = <Text style={constants.styles.bigBlue}>{name}</Text>
        return (
            <fieldset>
                <legend>What is {nameInStyle} your favourite fruit:</legend>
                <select value={selectedFruit} onChange={this.handleChange}>
                    {constants.FRUIT_OPTIONS.map((option) => (
                        <option key={option.value} value={option.value}>{option.label}</option>
                    ))}
                </select>
            </fieldset>
        );
    }
}
