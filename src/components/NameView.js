import React, {Component} from 'react';
import autoBind from 'react-autobind';
import BirthdayView from '../components/BirthdayView';

export default class NameView extends Component {
    constructor(props) {
        super(props);
        autoBind(this);
    }

    handleChange(e) {
        this.props.onNameChange(e.target.value);
    }

    render() {
        const name = this.props.clientName;
        const birthday = this.props.clientBirthday;
        console.log("name = "+{name})
        return (
            <fieldset>
                <legend>You personal data:</legend>
                What is your name:
                <input value={name}
                       onChange={this.handleChange}/>
                <p></p>
                <BirthdayView clientName = {name}
                              clientBirthday={birthday}
                              onBirthdayChange={this.props.onBirthdayChange}
                              />
            </fieldset>
        );
    }
}