import React, {Component} from 'react';
import autoBind from "react-autobind";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export default class  BirthView extends Component {
    constructor(props) {
        super(props);
        autoBind(this);
    }

    handleChange(value) {
        this.props.onBirthdayChange(value);
    }

    render() {
        const name = this.props.clientName;
        const birthday = this.props.clientBirthday;
        
        return (
            <fieldset>
                <legend> Select your {name} birthday:  </legend>
                <DatePicker selected={birthday}
                            onChange={(value, e) => this.handleChange(value)}
                            dateFormat="dd-MM-yyyy"/>
            </fieldset>
        );
    }
}