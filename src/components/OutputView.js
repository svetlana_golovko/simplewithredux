import React, {Component} from 'react';
import autoBind from 'react-autobind';
import { Text } from "react-native";
import * as constants from './constants';
import { format } from "date-fns";


export default class OutputView extends Component {
    constructor(props) {
        super(props);
        autoBind(this);
    }
    render() {
        const name = this.props.clientName;
        const selectedFruit = this.props.clientFruit;
        const clientBirthDay = this.props.clientBirthday;

        const birthDayFormated = format(clientBirthDay, "dd-MM-yyyy");
        const selectedFruitName = (selectedFruit === "0") ? " not selected" :
            constants.FRUIT_OPTIONS
                .filter((option) => option.value === selectedFruit)
                .map((option) => option.label);

        return (
            <fieldset>
                <legend>Dear Client:</legend>
                Your name is <Text style={constants.styles.bigBlue}>{name}</Text> <p/>
                Your favourite fruit is <Text style={constants.styles.bigBlue}>{selectedFruitName}</Text> <p/>
                Your birthday is on <Text style={constants.styles.bigBlue}>{birthDayFormated}</Text>
            </fieldset>
        );
    }
}