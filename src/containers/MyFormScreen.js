import React, { Component } from 'react';
import autoBind from 'react-autobind';
import { connect } from 'react-redux';
import * as selectors from '../store/reducers';
import * as actions from '../store/actions';
import './MyFormScreen.css';

import NameView from '../components/NameView';
import FruitView from '../components/FruitView';
import OutputView from '../components/OutputView';

class MyFormScreen extends Component {
    constructor(props) {
        super(props);
        autoBind(this);
    }

    handleNameChange(clientName) {
        this.props.dispatch(actions.changeClientName(clientName))
    }

    handleFruitChange(clientFruit) {
        this.props.dispatch(actions.changeFruit(clientFruit))
    }

    handleBirthdayChange = date => {
        this.props.dispatch(actions.changeBirthday(date))
    };


    render() {
        const clientName = this.props.clientName;
        const clientFruit = this.props.clientFruit;
        const clientBirthDay = this.props.clientBirthday;
        return (
            <div>
                <NameView
                    clientName={clientName}
                    clientBirthday={clientBirthDay}
                    onNameChange={this.handleNameChange}
                    onBirthdayChange={this.handleBirthdayChange}/>
                <FruitView
                    clientName={clientName}
                    clientFruit={clientFruit}
                    onFruitChange={this.handleFruitChange}
                />
                <OutputView clientName={clientName}
                            clientFruit={clientFruit}
                            clientBirthday={clientBirthDay} />
            </div>
        );
    }
}

// which props do we want to inject, given the global store state?
function mapStateToProps(state) {
    return {
        clientName: selectors.getSelectedName(state),
        clientFruit: selectors.getSelectedFruit(state),
        clientBirthday: selectors.getSelectedBirthday(state)
    };
}

export default connect(mapStateToProps)(MyFormScreen);