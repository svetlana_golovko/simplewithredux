import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import './index.css';
import MyFormScreen from './containers/MyFormScreen';
import reduce from './store/reducers';

export const store = createStore(reduce);

ReactDOM.render(
    <Provider store={store}>
            <MyFormScreen />
    </Provider>,
    document.getElementById('root')
);